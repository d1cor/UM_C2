#!/usr/bin/python

# Author: Diego Córdoba | @d1cor
# Contact: diego@juncotic.com | juncotic.com
# License: GPLv4


import subprocess, sys

comando = sys.argv[1]

proceso = subprocess.Popen([comando], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

out,err = proceso.communicate()

print("Salida: \n", out)
print("\nErrores: \n", err)

