#!/usr/bin/python

# Author: Diego Córdoba | @d1cor
# Contact: diego@juncotic.com | juncotic.com
# License: GPLv4


from url_celery import func
func(["http://duckduckgo.com", "https://juncotic.com", "https://mstdn.io", "https://diasp.eu"])

