#!/usr/bin/python

# Author: Diego Córdoba | @d1cor
# Contact: diego@juncotic.com | juncotic.com
# License: GPLv4



from celery import Celery

app = Celery('calc', broker='redis://localhost:6379', backend='redis://localhost:6379', include=['calc', 'mensajes'])


