#!/usr/bin/python

# Author: Diego Córdoba | @d1cor
# Contact: diego@juncotic.com | juncotic.com
# License: GPLv4



from calc_config import app

@app.task
def saludo():
    return "Todo ok, celery andando"
