#!/usr/bin/python

# Author: Diego Córdoba | @d1cor
# Contact: diego@juncotic.com | juncotic.com
# License: GPLv4


from calc_config import app
import time
@app.task
def verificar():
    time.sleep(20)
    return "Todo Ok, dale palante"
