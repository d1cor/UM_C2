#!/usr/bin/python

# Author: Diego Córdoba | @d1cor
# Contact: diego@juncotic.com | juncotic.com
# License: GPLv4



def suma(a, b):
    return a+b

def resta(a, b):
    return a-b

def mult(a, b):
    return a*b

def div(a, b):
    if b!=0:
        return a/b
    return 0

